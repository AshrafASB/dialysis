<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name'=>'super_admin',
            'email'=>'super_admin@super.com',
            'password'=>bcrypt('123456'),
            'email_verified_at'=>Carbon::now(),
//            'userType'=>'super_admin'
        ]);

        $nurse =  \App\User::create([
            'name'=>'nurse',
            'email'=>'nurse@gmail.com',
            'password'=>bcrypt('123456'),
            'email_verified_at'=>Carbon::now(),
            'userType'=>'nurse'
        ]);

        $doctor =  \App\User::create([
            'name'=>'doctor',
            'email'=>'doctor@gmail.com',
            'password'=>bcrypt('123456'),
            'email_verified_at'=>Carbon::now(),
            'userType'=>'doctor'
        ]);

        $user->attachRoles(['super_admin']);
        $nurse->attachRoles(['nurse']);
        $doctor->attachRoles(['doctor']);
    }
}
