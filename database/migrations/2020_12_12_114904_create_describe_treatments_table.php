<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescribeTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('describe_treatments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('classification_id')->unsigned();
            $table->text('treatment');
            $table->boolean('activate')->default(1);

            $table->foreign('classification_id')->references('id')->on('patients_classifications')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('describe_treatments');
    }
}
