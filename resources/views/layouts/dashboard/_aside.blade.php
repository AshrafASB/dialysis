<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar"
                    src="{{asset('dashboard_files/images/logo.jpeg')}}"
                    width="90px"
                    hight="100px"
                    alt="User Image">
        <div>
            <p class="app-sidebar__user-name">N:  {{isset(auth()->user()->name)?auth()->user()->name:''}}</p>
            <p class="app-sidebar__user-designation">R: {{implode(', ' , auth()->user()->roles->pluck('name')->toArray())}}</p>
        </div>
    </div>
    <ul class="app-menu">
        <li>
            <a class="app-menu__item active" href="{{route('dashboard.welcome')}}">
                <i class="app-menu__icon fa fa-dashboard"></i>
                <span
                    class="app-menu__label">{{ __('site.Dashboard')}}
                </span>
            </a>
        </li>

         <li>


{{--        @if(auth()->user()->hasPermission('read_WhoAreWe'))--}}
{{--            <li>--}}
{{--                <a class="app-menu__item " href="{{route('dashboard.WhoAreWes.index')}}">--}}
{{--                    <i class="app-menu__icon fa fa-list"></i>--}}
{{--                    <span--}}
{{--                        class="app-menu__label">{{ __('site.Who Are We')}}--}}
{{--                     </span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}
{{--        @if(auth()->user()->hasPermission('read_WhoAreWe'))--}}
{{--            <li>--}}
{{--                <a class="app-menu__item " href="{{route('dashboard.contact_us.index')}}">--}}
{{--                    <i class="app-menu__icon fa fa-list"></i>--}}
{{--                    <span--}}
{{--                        class="app-menu__label">{{ __('site.Contact Us')}}--}}
{{--                     </span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

        @if(auth()->user()->hasPermission('read_users'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.users.index')}}">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span
                        class="app-menu__label">{{ __('site.Users')}}
                </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_roles'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.roles.index')}}">
                    <i class="app-menu__icon fa fa-anchor"></i>
                    <span
                        class="app-menu__label">{{ __('site.Roles')}}
                </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_patients'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.patients.index')}}">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span
                        class="app-menu__label">{{ __('site.Request of Patients')}}
                </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_patients_classification'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.classifications.index')}}">
                    <i class="app-menu__icon fa fa-plus"></i>
                    <span
                        class="app-menu__label">{{ __('site.patients_classification')}}
                </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_describe_treatments') || auth()->user()->hasPermission('read_describe_treatments_for_nurse'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.describe_treatments.index')}}">
                    <i class="app-menu__icon fa fa-medkit"></i>
                    <span
                        class="app-menu__label">{{ __('site.Describe Treatment')}}
                </span>
                </a>
            </li>
        @endif



        @if(auth()->user()->hasPermission('read_settings')  )
            <li>
                <a class="app-menu__item " href="{{route('dashboard.settings.social_links')}}">
                    <i class="app-menu__icon fa fa-camera"></i>
                    <span
                        class="app-menu__label">{{ __('site.Social Links')}}
                </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_contact_us')  )
            <li>
                <a class="app-menu__item " href="{{route('dashboard.contact_us.index')}}">
                    <i class="app-menu__icon fa fa-wpforms"></i>
                    <span
                        class="app-menu__label">{{ __('site.contact_us')}}
                </span>
                </a>
            </li>
        @endif


{{--    @if(auth()->user()->hasPermission('read_settings'))--}}
{{--            <li class="treeview">--}}
{{--                <a class="app-menu__item" href="#" data-toggle="treeview">--}}
{{--                    <i class="app-menu__icon fa fa-laptop"></i>--}}
{{--                    <span class="app-menu__label">{{ __('site.Settings')}}</span>--}}
{{--                    <i class="treeview-indicator fa fa-angle-right"></i>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}
{{--                    <li>--}}
{{--                        <a class="treeview-item" href="#"><i class="icon fa fa-circle-o"></i>--}}
{{--                            {{ __('site.Pages Settings')}}--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li>--}}
{{--                        <a class="treeview-item" href="{{route('dashboard.settings.social_login')}}"--}}
{{--                           rel="noopener"><i class="icon fa fa-circle-o"></i> {{ __('site.Social Login')}}--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li>--}}
{{--                        <a class="treeview-item" href="{{route('dashboard.settings.social_links')}}"--}}
{{--                           rel="noopener"><i class="icon fa fa-circle-o"></i> {{ __('site.Social Links')}}--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--        @endif--}}



    </ul>
</aside>
