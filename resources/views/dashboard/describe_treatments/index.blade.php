@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> {{__('site.Describe Treatment')}} </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">Dashboard</a></li>
            <li class="breadcrumb-item"> {{__('site.Describe Treatment')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                {{-- this form for Search button                --}}
                <form action="" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" autofocus class="form-control" placeholder="Search" value="{{request()->search}}">
                            </div>
                        </div>{{-- end-of-col-4 --}}


                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Search</button>

                                    @if(auth()->user()->hasPermission('create_describe_treatments'))
                                        <a href="{{route('dashboard.describe_treatments.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @else
                                        <a href="#" disabled class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @endif
                            </div>
                        </div>{{-- end-of-col-4 --}}


                    </div>{{-- end-of-row --}}
                </form>{{-- end-of-form --}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}
        @if(auth()->user()->hasPermission('create_describe_treatments'))
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    @if($describe_treatments->count() > 0 )
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('site.Patient Name')}}</th>
                                <th>{{__('site.Treatment Described')}}</th>
                                <th>{{__('site.is Activated')}}</th>
                                <th>{{__('site.Created in')}}</th>
                                <th>{{__('site.Updated in')}}</th>
                                <th width="25%">{{__('site.action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($describe_treatments as $index=>$describe_treatment)
                                <tr>
                                    <td>{{++$index}}</td>
                                    <td>
                                        @foreach( $classifications as $classification )
                                            @if($describe_treatment->classification_id == $classification->id )
                                                {{$classification->patient->name}}
                                            @endif
                                        @endforeach


                                        </td>
                                    <td> {{\Illuminate\Support\Str::limit($describe_treatment->treatment, 50)}} </td>
                                    <td>
                                        @if($describe_treatment->activate == 0)
                                            <span style="display: inline-block" class="badge badge-danger">  False  </span>
                                        @elseif($describe_treatment->activate == 1)
                                            <span style="display: inline-block" class="badge badge-success">  True  </span>
                                        @endif

                                    </td>
                                    <td>{{$describe_treatment->created_at->diffForHumans()}}</td>
                                    <td>{{$describe_treatment->updated_at->diffForHumans()}}</td>
                                    <td>
                                        {{--show buttom--}}
                                        @if(auth()->user()->hasPermission('read_describe_treatments'))
                                            <a href="{{route('dashboard.describe_treatments.show', $describe_treatment->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> {{__('site.Details')}}</i></a>
                                        @else
                                            <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye"> {{__('site.Details')}}</i></a>
                                        @endif
                                        {{--Edit buttom--}}
                                        @if(auth()->user()->hasPermission('update_describe_treatments'))
                                            <a href="{{route('dashboard.describe_treatments.edit', $describe_treatment->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">Edit</i></a>
                                        @else
                                            <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                        @endif


                                        {{--Delete buttom--}}
                                        @if(auth()->user()->hasPermission('delete_describe_treatments'))
                                            <form action="{{route('dashboard.describe_treatments.destroy', $describe_treatment->id)}}" method="post" style="display: inline-block">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                            </form>
                                        @else
                                            <a href="#" disabled="" class="btn btn-danger btn-sm"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                        {{$describe_treatments->appends(request()->query())->links()}}
                    @else
                        <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                    @endif
                </div>
            </div>
        @elseif(auth()->user()->hasPermission('create_describe_treatments_for_nurse'))
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    @if($describe_treatments->count() > 0 )
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('site.Patient Name')}}</th>
                                <th>{{__('site.Treatment Described')}}</th>
                                <th>{{__('site.is Activated')}}</th>
                                <th>{{__('site.Created in')}}</th>
                                <th>{{__('site.Updated in')}}</th>
                                <th width="25%">{{__('site.action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($describe_treatments as $index=>$describe_treatment)
                                @if($describe_treatment->activate == 1)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>
                                            @foreach( $classifications as $classification )
                                                @if($describe_treatment->classification_id == $classification->id )
                                                    {{$classification->patient->name}}
                                                @endif
                                            @endforeach


                                        </td>
                                        <td> {{\Illuminate\Support\Str::limit($describe_treatment->treatment, 50)}} </td>
                                        <td>
                                            @if($describe_treatment->activate == 0)
                                                <span style="display: inline-block" class="badge badge-danger">  False  </span>
                                            @elseif($describe_treatment->activate == 1)
                                                <span style="display: inline-block" class="badge badge-success">  True  </span>
                                            @endif

                                        </td>
                                        <td>{{$describe_treatment->created_at->diffForHumans()}}</td>
                                        <td>{{$describe_treatment->updated_at->diffForHumans()}}</td>
                                        <td>
                                            {{--show buttom--}}
                                            @if(auth()->user()->hasPermission('read_describe_treatments_for_nurse'))
                                                <a href="{{route('dashboard.describe_treatments.show', $describe_treatment->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> {{__('site.Details')}}</i></a>
                                            @else
                                                <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye"> {{__('site.Details')}}</i></a>
                                            @endif

                                        </td>
                                    </tr>
                                @endif
                            @endforeach

                            </tbody>

                        </table>
                        {{$describe_treatments->appends(request()->query())->links()}}
                    @else
                        <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                    @endif
                </div>
            </div>

        @endif
    </div>{{--end-of-tile mb-4--}}


@endsection
