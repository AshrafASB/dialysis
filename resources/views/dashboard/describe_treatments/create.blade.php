@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($describe_treatment)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Describe Treatment')}}
                    </i>
               </h1>
            @elseif(isset($details))
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Details Describe Treatment')}}
                    </i>
                </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Describe Treatment') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.describe_treatments.index')}}">{{__('site.Describe Treatment')}}</a></li>
            @if(isset($describe_treatment))
                <li class="breadcrumb-item">{{__('site.Update Describe Treatment')}}</li>
            @elseif(isset($details))
                <li class="breadcrumb-item">{{__('site.Details Describe Treatment')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Describe Treatment')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($describe_treatment)?route('dashboard.describe_treatments.update',$describe_treatment->id):route('dashboard.describe_treatments.store')}}" method="post">
                 @csrf
                 @if(isset($describe_treatment))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')


                 @if(isset($describe_treatment))
                     {{-- Select of Patient --}}
                     <div class="form-group">
                         <label>{{__('site.Patient Name')}} :</label>
                         <select name="classification_id" class="form-control">
                             @foreach( $patients as $patient )

                                 <option value="{{$describe_treatment->classification_id}}" {{$patient->id == $describe_treatment->classification_id?"SELECTED":""}}> {{ $patient->name }}</option>
                             @endforeach
                         </select>
                     </div>
                 @elseif(isset($details))
                     {{-- Select of Patient --}}
                     <div class="form-group">
                         <label>{{__('site.Patient Name')}} :</label>
                         <select name="classification_id" class="form-control" disabled>
                             @foreach( $patients as $patient )

                                 <option value="{{$details->classification_id}}" {{$patient->id == $details->classification_id?"SELECTED":""}}> {{ $patient->name }}</option>
                             @endforeach
                         </select>
                     </div>
                 @else
                     {{-- Select of Patient --}}
                     <div class="form-group">
                         <label>{{__('site.Patient Name')}} :</label>
                         <select name="classification_id" class="form-control">
                             @foreach( $classifications as $classification )
                                 <option value="{{$classification->patient->id}} "> {{ $classification->patient->name }}</option>
                             @endforeach
                         </select>
                     </div>

                 @endif

                 <div class="form-group">
                     <label>{{__('site.Treatment')}} :</label>
                     <textarea name="treatment" cols="30" {{isset($details) ?"disabled":''}} rows="10"  class="form-control">{{isset($describe_treatment)?$describe_treatment->treatment:""}} {{isset($details)?$details->treatment:""}}</textarea>
{{--                     <input type="text" name="description" class="form-control" value="{{isset($describe_treatment)?$describe_treatment->description:""}}">--}}
{{--                     <textarea name="treatment" cols="30" rows="10"  class="form-control">{{isset($details)?$details->treatment:""}}</textarea>--}}
                 </div>


                 @if(isset($describe_treatment))
                     <div class="form-group">
                         <label>{{__('site.is Activated')}} :</label>
                         <select name="activate" class="form-control">
                                 <option value="0" {{$describe_treatment->activate == 0 ?"selected":''}}> False</option>
                                 <option value="1" {{$describe_treatment->activate == 1 ?"selected":''}}> True</option>
                         </select>
                     </div>
                 @elseif(isset($details))
                     <div class="form-group">
                         <label>{{__('site.is Activated')}} :</label>
                         <select name="activate" class="form-control" disabled>
                             <option value="0" {{$details->activate == 0 ?"selected":''}}> False</option>
                             <option value="1" {{$details->activate == 1 ?"selected":''}}> True</option>
                         </select>
                     </div>
                 @else
                     <div class="form-group">
                         <label>{{__('site.is Activated')}} :</label>
                         <select name="activate" class="form-control">
                             <option value="0"> False</option>
                             <option value="1">True </option>
                         </select>
                     </div>
                 @endif

                     @if( isset($describe_treatment) )
                         <div class="form-group">
                         <button type="submit" class="btn btn-primary">
                                 <i class="fa fa-edit"></i>
                                 {{__('site.Update')}}
                         </button>
                     </div>
                     @elseif(isset($details))
                         <a class="btn btn-primary " href="{{route('dashboard.describe_treatments.index')}}">
                             <i class="fa fa-circle-o"></i>
                             {{__('site.Back')}}
                         </a>
                     @else
                           <div class="form-group">
                             <button type="submit" class="btn btn-primary">
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                             </button>
                          </div>
                     @endif

             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
