<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">

    <title>Dialysis</title>
    <!--
    SOFTY PINKO
    https://templatemo.com/tm-535-softy-pinko
    -->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/bootstrap.min.css")}}">

    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/font-awesome.css")}}">

    <link rel="stylesheet" href="{{asset("assets/css/templatemo-softy-pinko.css")}}">

</head>

<body>

<!-- ***** Preloader Start ***** -->
<div id="preloader">
    <div class="jumper">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- ***** Preloader End ***** -->


<!-- ***** Welcome Area Start ***** -->
<div class="welcome-area" id="welcome">
    <!-- ***** Header Text Start ***** -->
    <div class="header-text">
        <div class="container">
            <div class="row">
                <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                    <h1>Thanks <strong>You</strong><br>for Contact us<strong> ^-^ </strong></h1>
                    <p>We will respond to your inquiries with you as soon as possible</p>
                    <a href="{{route('home')}}" class="main-button-slider">Back to Main Page</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->




<!-- ***** Features Small Start ***** -->
<section class="section home-feature">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

            </div>
        </div>
    </div>
</section>
<!-- ***** Features Small End ***** -->


<!-- ***** Footer Start ***** -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <ul class="social">
                    <li><a href="{{setting('facebook_link')}}"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="{{setting('twitter_link')}}"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="{{setting('instagram_link')}}"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="{{setting('whatsUp_link')}}"><i class="fa fa-whatsapp"></i></a></li>
                    <li><a href="{{setting('youtubeChanel_link')}}"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="copyright">{{setting('CopyRight_link')}}</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="{{asset("assets/js/jquery-2.1.0.min.js")}}"></script>

<!-- Bootstrap -->
<script src="{{asset("assets/js/popper.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<!-- Plugins -->
<script src="{{asset("assets/js/scrollreveal.min.js")}}"></script>
<script src="{{asset("assets/js/waypoints.min.js")}}"></script>
<script src="{{asset("assets/js/jquery.counterup.min.js")}}"></script>
<script src="{{asset("assets/js/imgfix.min.js")}}"></script>

<!-- Global Init -->
<script src="{{asset("assets/js/custom.js")}}"></script>


</body>
</html>
