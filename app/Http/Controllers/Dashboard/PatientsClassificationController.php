<?php

namespace App\Http\Controllers\Dashboard;

use App\Patient;
use App\Http\Controllers\Controller;

use App\PatientsClassification;
use Illuminate\Http\Request;

class PatientsClassificationController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.classifications.";

        //Permissions
        $this->middleware('permission:read_patients_classification')->only(['index']);
        $this->middleware('permission:create_patients_classification')->only(['create','store']);
        $this->middleware('permission:update_patients_classification')->only(['edit','update']);
        $this->middleware('permission:delete_patients_classification')->only(['destroy']);



    }

    public function index()
    {
        $classifications = PatientsClassification::WhenSearch(request()->search)
            ->with(['patients','patient'])
            ->paginate(5);
        return view($this->path.'index',compact('classifications'));
    }//end of index

    public function create()
    {
//        $classifications = PatientsClassification::all();
        $patients = Patient::all();
        return view($this->path.'create' , compact('patients'));
    }//end of create

    public function store(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'patient_id' => 'required|unique:patients_classifications,patient_id',
        ]);
        PatientsClassification::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(PatientsClassification $classification)
    {
        $patients = Patient::all();
        return view($this->path.'create',compact(['classification' , 'patients']));
    }//end of edit

    public function update(Request $request, PatientsClassification $classification)
    {
        $request->validate([
            'patient_id' => 'required|unique:patients_classifications,patient_id,'.$classification->id,
        ]);
        $classification->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(PatientsClassification $classification)
    {
        $classification->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
