<?php

namespace App\Http\Controllers\Dashboard;

use App\DescribeTreatment;
use App\Http\Controllers\Controller;
use App\Patient;
use App\PatientsClassification;
use Illuminate\Http\Request;

class DescribeTreatmentController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.describe_treatments.";

        //Permissions
        $this->middleware('permission:read_describe_treatments')->only(['index','show']);
        $this->middleware('permission:create_describe_treatments')->only(['create','store']);
        $this->middleware('permission:update_describe_treatments')->only(['edit','update']);
        $this->middleware('permission:delete_describe_treatments')->only(['destroy']);

        $this->middleware('permission:read_describe_treatments_for_nurse')->only(['index']);
        $this->middleware('permission:create_describe_treatments_for_nurse')->only(['create','store']);
        $this->middleware('permission:update_describe_treatments_for_nurse')->only(['edit','update']);
        $this->middleware('permission:delete_describe_treatments_for_nurse')->only(['destroy']);

    }

    public function index()
    {
        $describe_treatments = DescribeTreatment::WhenSearch(request()->search)->orderBy('id', 'DESC')
            ->paginate(5);

        $classifications = PatientsClassification::WhenSearch(request()->search)->with(['patients','patient'])->paginate(5);
        return view($this->path.'index',compact(['describe_treatments' , 'classifications']));
    }//end of index

    public function create()
    {
        $patients = Patient::all();
        $classifications = PatientsClassification::all();
        return view($this->path.'create',compact('patients','classifications'));
    }//end of create

    public function store(Request $request)
    {
//        $request->validate([
//            'classification_id' => 'required|unique:describe_treatments,classification_id',
//        ]);
        DescribeTreatment::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show(DescribeTreatment $describe_treatment)
    {
        $patients = Patient::all();
        $details = $describe_treatment;
        return view($this->path.'create',compact('details','patients'));

    }//end of show

    public function edit(DescribeTreatment $describe_treatment)
    {
        $patients = Patient::all();
        return view($this->path.'create',compact('describe_treatment','patients'));
    }//end of edit

    public function update(Request $request, DescribeTreatment $describe_treatment)
    {
//        $request->validate([
//            'classification_id' => 'required|unique:describe_treatments,classification_id,'.$describe_treatment->id,
//        ]);
        $describe_treatment->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(DescribeTreatment $describe_treatment)
    {
        $describe_treatment->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
