<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.patients.";

        //Permissions
        $this->middleware('permission:read_patients')->only(['index']);
        $this->middleware('permission:create_patients')->only(['create','store']);
        $this->middleware('permission:update_patients')->only(['edit','update']);
        $this->middleware('permission:delete_patients')->only(['destroy']);

    }

    public function index()
    {
        $patients = Patient::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('patients'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:patients,email',
        ]);
        Patient::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Patient $patient)
    {
        return view($this->path.'create',compact('patient'));
    }//end of edit

    public function update(Request $request, Patient $patient)
    {
        $request->validate([
            'email' => 'required|unique:patients,email,'.$patient->id,
        ]);
        $patient->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Patient $patient)
    {
        $patient->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
