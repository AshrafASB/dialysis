<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [

        'super_admin' => [
            'users' => 'c,r,u,d',

            'roles'=>'c,r,u,d',
            'settings'=>'c,r,u,d',
            'contact_us'=>'c,r,u,d',

            'patients' => 'c,r,u,d',
            'patients_classification' => 'c,r,u,d',
            'describe_treatments' => 'c,r,u,d',
            'describe_treatments_for_nurse' => 'c,r,u,d',
        ],

        'doctor' => [
            'patients' => 'c,r,u,d',
            'patients_classification' => 'c,r,u,d',
            'describe_treatments' => 'c,r,u,d',

        ],

        'nurse' => [
            'describe_treatments' => 'r',
            'patients' => 'c,r,u,d',
            'patients_classification' => 'c,r,u,d',
            'describe_treatments_for_nurse' => 'c,r,u,d',
        ],

    ],
//    'permission_structure'=>[
//
//    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
